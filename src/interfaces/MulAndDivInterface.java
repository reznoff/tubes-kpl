package interfaces;

public interface MulAndDivInterface extends PowInterface {
	public double mul(double n1, double n2);
	public double div(double n1, double n2);
}
