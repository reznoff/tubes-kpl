package main;

import classes.C;
import classes.D;
import classes.E;

public class MainApp {

	public static void main(String[] args) {
		
		C c = new C();
		c.add();
		c.sub();
		c.mul();
		c.div();
		
		D d = new D();
		d.toString();
		
		E e = new E();
		e.contains("A");
		e.toString();
	}
}
