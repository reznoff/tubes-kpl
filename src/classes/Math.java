package classes;

public class Math {
	
	public int mod(int n1, int n2) {
		return java.lang.Math.floorMod(n1, n2);
	}
	
	public double sqrt(double n) {
		return java.lang.Math.sqrt(n);
	}
}
