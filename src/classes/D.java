package classes;

import interfaces.PowInterface;

public class D implements PowInterface {

	public D() {
		System.out.println("CTor di kelas "+getClass().getSimpleName());
	}
	
	@Override
	public double pow(double n1, double n2) {
		return java.lang.Math.pow(n1, n2);
	}
	
	@Override
	public String toString() {
		return "toString di kelas "+getClass().getSimpleName();
	}

}
