package test.java;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import classes.C;

public class CTest {

	private C c;
	
	@Before
	public void setUp() throws Exception {
		c = new C();
	}

	@Test
	public void addTest() {
		int hasil = c.add();
		assertEquals(0, hasil);
	}
	
	@Test 
	public void subTest() {
		int hasil = c.sub();
		assertEquals(0, hasil);
	}
	
	@Test
	public void mulTest() {
		double hasil = c.mul();
		assertEquals(0, hasil, 0.00);
	}
	
	@Test
	public void divTest() {
		double hasil = c.div();
		assertEquals(Double.NaN, hasil, 0.00);
	}
	
	@Test
	public void modTest() {
		int hasil = c.mod(6, 5);
		assertEquals(1, hasil);
	}
	
	@Test
	public void rootTest() {
		double hasil = c.sqrt(81);
		assertEquals(9, hasil, 0.00);
	}
	
	@Test
	public void toStringTest() {
		String output = "toString di kelas C";
		assertEquals(output, c.toString());
	}

}
