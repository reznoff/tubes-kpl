import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import classes.D;

public class DTest {
	
	private D d;

	@Before
	public void setUp() throws Exception {
		d = new D();
	}

	@Test
	public void powTest() {
		double hasil = d.pow(4, 4);
		assertEquals(256, hasil, 0.00);
	}
	
	@Test
	public void toStringTest() {
		String output = "toString di kelas D";
		assertEquals(output, d.toString());
	}

}
