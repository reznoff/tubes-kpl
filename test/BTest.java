import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import classes.B;


//Batas toleransi actual result dengan expectation adalah 0.01 (delta)
public class BTest {

	private B b;
	
	@Before
	public void setUp() throws Exception {
		b = new B();
	}

	@Test
	public void powTest() {
		double hasil = b.pow(9, 2);
		assertEquals(81, hasil, 0.01);
	}
	
	@Test
	public void mulTest() {
		double hasil = b.mul(4, 4);
		assertEquals(16, hasil, 0.01);
	}
	
	@Test
	public void divTest() {
		double hasil = b.div(9, 2);
		assertEquals(4.5, hasil, 0.01);
	}
	
	@Test
	public void toStringTest() {
		String output = "toString di kelas B";
		assertEquals(output, b.toString());
	}

}
