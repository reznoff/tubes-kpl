import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import classes.A;

public class ATest {
	
	private A a;
	
	@Before
	public void setUp() throws Exception {
		a = new A();
	}

	@Test
	public void addTest() {
		int hasil = a.add(3, 5);
		assertEquals(8, hasil);
	}
	
	@Test
	public void subTest() {
		int hasil = a.sub(5, 3);
		assertEquals(2, hasil);
	}
	
	@Test
	public void toStringTest() {
		String output = "toString di kelas A";
		assertEquals(output, a.toString());
	}
	
}
