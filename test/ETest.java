import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import classes.E;

public class ETest {

	private E e;
	private List<String> dummyList = new ArrayList<String>();
	
	@Before
	public void setUp() throws Exception {
		e = new E();
		dummyList.add("Dumbest Data");
	}

	@Test
	public void containTest() {
		assertTrue(e.contains("Dummy Data"));
	}
	
	//Setter Getter tidak butuh unit test
	
	@Test
	public void toStringTest() {
		String output = "toString di kelas E";
		assertEquals(output, e.toString());
	}


}
